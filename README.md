# infra

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/brie/infra?branch=main)

I have collected enough domain names over the years that continuing to manage DNS records by hand is rather annoying. In this project, I pubish the configs used to manage a subset of the domains via Infrastructure as Code (IaC). 

This includes Terraform for managing records with these DNS providres:

  - Cloudflare
  - Google Cloud DNS
