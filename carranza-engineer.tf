resource "google_dns_record_set" "cname-netlify-brie-carranza-engineer" {
  name         = "brie.carranza.engineer."
  managed_zone = var.carranzazonename
  type         = "CNAME"
  ttl          = 299

  rrdatas = ["affectionate-liskov-a9f3cd.netlify.app."]
}


resource "google_dns_record_set" "a-for-carranzadotengineer" {
  name         = "carranza.engineer."
  managed_zone = var.carranzazonename
  type         = "A"
  ttl          = 300

  rrdatas = ["104.198.14.52"]
}

resource "google_dns_record_set" "a-for-gitlabdotcarranzadotengineer" {
  name         = "gitlab.carranza.engineer."
  managed_zone = var.carranzazonename
  type         = "A"
  ttl          = 300

  rrdatas = ["127.0.0.1"]
}








