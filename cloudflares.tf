data "cloudflare_zone" "this" {
  name = "termmarc.com"
}

resource "cloudflare_record" "foobar" {
  zone_id = data.cloudflare_zone.this.id
  name    = "foobar"
  value   = "127.0.0.1"
  type    = "A"
  proxied = false
}

resource "cloudflare_record" "www" {
  zone_id = data.cloudflare_zone.this.id
  name    = "www"
  value   = "127.0.0.1"
  type    = "A"
  proxied = false
}


resource "cloudflare_record" "california" {
  zone_id = data.cloudflare_zone.this.id
  name    = "california"
  value   = "1.1.1.1"
  type    = "A"
  proxied = false
}

