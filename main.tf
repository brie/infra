provider "google" {
  project = var.project_id
  region  = "us-central1"
  zone    = "us-central1-c"
}

provider "google-beta" {
  project = "briedev"
  region  = "us-central1"
  zone    = "us-central1-c"
}


resource "google_dns_record_set" "a-sunflower" {
  name         = "sunflower.gallery."
  managed_zone = var.sunflowergallery
  type         = "A"
  ttl          = 300

  rrdatas = ["151.101.65.195", "151.101.1.195"]
}

resource "google_dns_record_set" "www-sunflower" {
  name         = "www.sunflower.gallery."
  managed_zone = var.sunflowergallery
  type         = "A"
  ttl          = 300

  rrdatas = ["151.101.65.195", "151.101.1.195"]
}

