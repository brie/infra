resource "google_dns_record_set" "a" {
  name         = "backend.unicorns.run."
  managed_zone = var.uzonename
  type         = "A"
  ttl          = 300

  rrdatas = ["127.0.0.1"]
}

resource "google_dns_record_set" "frontend-unicorns" {
  name         = "frontend.unicorns.run."
  managed_zone = var.uzonename
  type         = "A"
  ttl          = 300

  rrdatas = ["127.0.0.1"]
}

resource "google_dns_record_set" "gitlab-unicorns" {
  name         = "gitlab.unicorns.run."
  managed_zone = var.uzonename
  type         = "A"
  ttl          = 300

  rrdatas = ["127.0.0.1"]
}
