/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "Project id where the zone will be created."
  default     = "briedev"
}

variable "name" {
  description = "DNS zone name."
  default     = "unicorns.run."
}

variable "domain" {
  description = "Zone domain."
  default     = "unicorns.run"
}

variable "uzonename" {
  description = "The description..."
  default     = "unicorns-run"
}

variable "carranzazonename" {
  description = "The human-friendly name for the carranza.engineer. zone."
  default     = "carranza-engineer"
}


variable "sunflowergallery" {
  description = "The name of the zone"
  default     = "sunflower-gallery"
}



variable "labels" {
  type        = map(any)
  description = "A set of key/value label pairs to assign to this ManagedZone"
  default = {
    owner   = "foo"
    version = "bar is a cute cat"
  }
}

variable "cloudflare_email" {
  type        = string
  description = "Cloudflare email address"
  default = "brie.carranza@gmail.com"
}

